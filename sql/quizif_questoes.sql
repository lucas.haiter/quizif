-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: quizif
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `questoes`
--

DROP TABLE IF EXISTS `questoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `questoes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `descricao` varchar(1024) DEFAULT NULL,
  `disciplina` varchar(50) DEFAULT NULL,
  `dificuldade` varchar(50) DEFAULT NULL,
  `opcaoa` varchar(100) DEFAULT NULL,
  `opcaob` varchar(100) DEFAULT NULL,
  `opcaoc` varchar(100) DEFAULT NULL,
  `opcaod` varchar(100) DEFAULT NULL,
  `opcaoe` varchar(100) DEFAULT NULL,
  `opcaocorreta` varchar(1) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questoes`
--

LOCK TABLES `questoes` WRITE;
/*!40000 ALTER TABLE `questoes` DISABLE KEYS */;
INSERT INTO `questoes` VALUES (1,'Quais são os quatro países da África?','ddd','geografia','media','1.Sudão 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Nigéria 3.Chade 4.África do Sul','1.Sudão 2.Botsuana 3.Lima 4.África do Sul','1.Tanzania 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Argentina 3.Afeganistão 4.África do Sul','b','2023-08-29 18:23:08'),(2,'Quais são os quatro países da África?','dddd','geografia','media','1.Sudão 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Nigéria 3.Chade 4.África do Sul','1.Sudão 2.Botsuana 3.Lima 4.África do Sul','1.Tanzania 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Argentina 3.Afeganistão 4.África do Sul','b','2023-08-29 18:23:11'),(3,'André Bordignon?','andre 1 + andre 2','historia','facil','1','2','3','4','5','d','2023-08-29 18:27:00'),(4,'NOVINHO','ssss','geografia','media','1.Sudão 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Nigéria 3.Chade 4.África do Sul','1.Sudão 2.Botsuana 3.Lima 4.África do Sul','1.Tanzania 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Argentina 3.Afeganistão 4.África do Sul','b','2023-08-29 18:33:05'),(5,'Quais são os quatro países da África?','.klhulukyukyu','geografia','media','1.Sudão 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Nigéria 3.Chade 4.África do Sul','1.Sudão 2.Botsuana 3.Lima 4.África do Sul','1.Tanzania 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Argentina 3.Afeganistão 4.África do Sul','b','2023-08-29 18:59:47'),(6,'Quais são os quatro países da África?','dddd','geografia','media','1.Sudão 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Nigéria 3.Chade 4.África do Sul','1.Sudão 2.Botsuana 3.Lima 4.África do Sul','1.Tanzania 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Argentina 3.Afeganistão 4.África do Sul','b','2023-08-29 18:59:49'),(7,'rgfrfrwfe','fffdsfd','geografia','media','1.Sudão 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Nigéria 3.Chade 4.África do Sul','1.Sudão 2.Botsuana 3.Lima 4.África do Sul','1.Tanzania 2.Nigéria 3.Afeganistão 4.África do Sul','1.Sudão 2.Argentina 3.Afeganistão 4.África do Sul','b','2023-09-05 17:55:53'),(8,'','','','','','','','','','','2023-09-05 17:57:54'),(9,'pEDRO','lllaaalalalalal','logica','superfacil','popopo','papapapa','pepeepepep','pipiipip','pupupupuup','b','2023-09-05 18:14:11');
/*!40000 ALTER TABLE `questoes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-05 15:18:53
